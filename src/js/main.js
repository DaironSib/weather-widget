//Частота обновления прогноза
const weatherTimeOut = 300000

//Функция перевода Кельвинов в Цельсии
function convKelvinCelsius(val) {
    return Math.round(val - 273)
}

//Функция получения данных прогноза
async function getWeatherData() {
    //Определяем какой выбран город
    const cityID = document.getElementById("citySelect").value
    //Получаем шаблон записи прогноза погоды
    const template = document.querySelector('#weatherWidgetItem').content

    //Удаляем старый вывод прогноза погоды
    if (document.getElementsByClassName('itemCont').length > 0) {
        while(document.getElementsByClassName('itemCont').length != 0) {
            document.querySelector('.widgetBody').removeChild(document.getElementsByClassName('itemCont')[0])
        }
    }

    try {
        const response = await fetch('http://api.openweathermap.org/data/2.5/forecast?id=' + cityID + '&APPID=12bd4b3abdb4352f887d2c1fe1492536', {
            method: 'GET'
        })

        const weatherData = await response.json()

        if (response.status !== 200) alert(weatherData.message)
        else {
            //Перебираем прогноз за сутки
            weatherData.list.slice(0, 8).forEach(function(item, i) {
                //Рендерим DOM
                template.querySelector('.date').textContent = weatherData.list[i].dt_txt
                template.querySelector('.temp span').textContent = convKelvinCelsius(weatherData.list[i].main.temp)
                template.querySelector('.wind span').textContent = parseInt(weatherData.list[i].wind.speed)
                document.querySelector('.widgetBody').appendChild(template.cloneNode(true))
            });
        }
    } catch (err) {
        alert(err.message)
    }

    console.log('Обновили данные')
}

getWeatherData()
setInterval('getWeatherData()', weatherTimeOut)